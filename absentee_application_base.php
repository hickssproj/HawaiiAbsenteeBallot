
<!DOCTYPE html>
<html>
<head>
<title>Application for Hawaii Absentee Voter Ballot</title>
<style type="text/css" title="text/css" media="all">
.error {
	color: red;
	font-weight: bold;
}
</style>
<script type="text/javascript">
var debug = false;
/*
 * validate a social security number in the form ddd-dd-dddd
 * give an alert if invalid
 */
function validateSSN(ssn) {
	if (! ssn.match(/^\d{3}-\d{2}-\d{4}$/)) {
		window.alert('Invalid Social Security Number. Must be 9 digits in the form ddd-dd-dddd.');
		return false;
	}
	return true;
}

/* Adds/Removes days appropriate to what Month was selected.
 * @param selectedMonth The numerical equivalent of the selected month.
 */
function addRemoveDays(selectedMonth) {
  var days = document.getElementById('DOBday');
  var currentNumDays = days.length;
  var expectedNumDays;
  // Expect 29 for February
  if ( selectedMonth == 2 ) {
    expectedNumDays = 29;
  }
  // If April, June, September, November, set expected to 30.
  else if ( (selectedMonth == 4) || (selectedMonth == 6) || (selectedMonth == 9) || (selectedMonth == 11) ) {
    expectedNumDays = 30;
  } else { // Set to default 31 for all other months.
    expectedNumDays = 31;
  }
  
  // Determine if adding or removing days
  if (expectedNumDays >= currentNumDays) {
    addDays(expectedNumDays, currentNumDays);
  } else {
    removeDays(expectedNumDays, currentNumDays);
  }
}

/* Adds amount of days appropriate for the selected month. */
function addDays(expectedNumDays, currentNumDays) {
  // Keep adding days until currentNum is equal to expected.
  while (expectedNumDays > currentNumDays) {
    var newDay = document.createElement('option');
    newDay.text = currentNumDays + 1;
    newDay.value = currentNumDays + 1;
    var days = document.getElementById('DOBday');
    days.add(newDay);
	currentNumDays++;
  }
}

// Removes amount of days appropriate for the selected month.
function removeDays(expectedNumDays, currentNumDays) {
  var days = document.getElementById('DOBday');
  while (expectedNumDays < currentNumDays) {
    days.remove(days.length - 1);
	currentNumDays--;
  }
}

/*
 * validate a telephone number in the form ddd-dddd
 * give an alert if invalid
 */
function validateTelephone(tel, alert) {
	if (! tel.match(/^\d{3}-\d{4}$/)) {
		if (alert) window.alert('Invalid Telephone Number. Must be 7 digits in the form ddd-dddd.');
		return false;
	}
	return true;
}
/*
 * validate a Name which must be all alphabetic characters
 * give an alert if invalid
 */
function validateName(name, alert) {
	if (! name.match(/^[A-Za-z ,\.]+$/)) {
		if (alert) window.alert('Invalid Name. Must be alphabetic characters A-Z, a-z, spaces, commas, or periods.');
		return false;
	}
	return true;
}
/*
 * validate a zip code in the form ddddd or ddddd-dddd
 * give an alert if invalid
 */
function validateHIZipCode(zip, alert) {
	if (! zip.match(/^96[78]\d\d(-\d{4})?$/)) {
		if (alert) window.alert('Invalid Zip Code. Must be 5 digits in the form ddddd or 9 digits in the form ddddd-dddd and begin with HI prefixes 967 or 968.');
		return false;
	}
	return true;
	}
/*
 * Check that all sections are filled in and the affirmation checkbox has been checked.
 */
 function validateFilled() {
	 var foundIt = false;
	 if (debug) window.alert("document.form.election.length is "+document.form.election.length);
	 for (var i=0; i<document.form.election.length; i++) {
		 if (document.form.election[i].checked) foundIt = true;
	 }
	 if (debug) window.alert("foundIt is "+foundIt);
	 if (! foundIt){
		 window.alert("Please select an election type.");
		 return false;
	 }
	 if ( ! validateSSN(document.form.socialSecurityNumber.value) )
		 return false;
	 if ( ! document.form.gender[0].checked && ! document.form.gender[1].checked ) {
		 window.alert("Please check Male or Female.");
		 return false;
	 }
	 if ( ! validateTelephone(document.form.homePhone.value, false) &&
		  ! validateTelephone(document.form.businessPhone.value, false) ) {
			 window.alert("Please enter a valid Telephone.");
			 return false;
	 }
	 if ( ! validateName(document.form.firstName.value, true) ||
		  ! validateName(document.form.lastName.value, true) ) {
			 return false;
	 }
	 if (debug) window.alert("residence address is "+document.form.residenceAddress.value);
	 if ( ! (document.form.residenceAddress.value && validateName(document.form.residenceCityTown.value, false)
			 && validateHIZipCode(document.form.residenceZipCode.value, false) ) &&
		  ! (document.form.residenceDescription.value && validateName(document.form.descriptionCityTown.value, false)
			 && validateHIZipCode(document.form.descriptionZipCode.value, false) ) ) {
		 window.alert("Please enter a valid address for item 6 or 8");
		 return false;
	 }
	 if (document.form.election[0].checked || document.form.election[2].checked || document.form.election[3].checked) {
		 if (debug) window.alert("election 0, 2 or 3 checked");
		 if ( ! validateName(document.form.mailToPrimaryName.value, true) ) {
			 window.alert("Please enter a valid Name for item 9");
			 return false;
		 }
		 if ( ! document.form.mailToPrimaryAddress.value) {
			 window.alert("Please enter a valid Address for item 10");
			 return false;
		 }
	 }
	 if (document.form.election[1].checked || document.form.election[3].checked) {
		 if (debug) window.alert("election 1 or 3 checked");
		 if ( ! validateName(document.form.mailToGeneralName.value, false) ) {
			 window.alert("Please enter a valid Name for item 11");
			 return false;
		 }
		 if ( ! document.form.mailToGeneralAddress.value ) {
			 window.alert("Please enter a valid Address for item 12");
			 return false;
		 }
	 }
	 if (! document.getElementById("affirm").value) {
		 window.alert("Please check the affirmation box above the Submit button.");
		 return false;
	 }
	 return true;
}
 function copyAddress() {
	 document.form.mailToGeneralName.value = document.form.mailToPrimaryName.value;
	 document.form.mailToGeneralAddress.value = document.form.mailToPrimaryAddress.value;
	 
 }
 <!-- The following php functions are used to validate the various components of the application. -->
 </script>

<!-- Make Sticky helper function. Makes inputs sticky. -->
<?php 
	 $errType = "";
	 $submitted = 0;
	function makeSticky($name){
		if (isset($_POST[$name])){
			echo "value=\"".$_POST[$name]."\"";
		}
		else {
			echo "value=\"\"";
		}	
	}
	
	function makeError($name){
		global $errType;
		global $submitted;
		if ($errType == $name && $submitted == 1){
			echo 'class="error"';
		}
	}
?>

</head>
<body>
<!-- PHP variable stores if an error occurred and sets the variable to 1 -->

<form name="form" action="absentee_application_base.php" method="post" onSubmit="">
<h1>Section I.</h1>
<p>
I hereby request Absentee Ballots for the following Election(s):
</p>

<div <?php makeError("election") ?>
<!-- Ensures atleast 1 election has been selected. -->
<?php 
	if (! isset($_POST["election"])){
		$errType = "election";
		echo "Please select one of the following:";
}
?>
</div>

<!-- Makes selected election type sticky if selected -->
<input type="radio" name="election" value="primary-only" 
<?php if (isset($_POST["election"])){if($_POST["election"] == isset($_POST["election"])){echo "checked";}}?> /> Primary Only

<input type="radio" name="election" value="general-only" 
<?php if (isset($_POST["election"])){if($_POST["election"] == "general-only"){echo "checked";}}?>/> General Only

<input type="radio" name="election" value="primary-and-general" 
<?php if (isset($_POST["election"])){if($_POST["election"] == "primary-and-general"){echo "checked";}}?>/> Primary &amp; General

<input type="radio" name="election" value="special-elections" 
<?php if (isset($_POST["election"])){if($_POST["election"] == "special-elections"){echo "checked";}}?>/>  Special Elections

<p>
 I hereby request ballot instructions in:
</p>


<!-- Generates languages checkboxes -->
<?php 
	$languages = Array("Chinese","Japanese","Ilocano");
	$i = 0;
	while ($i < count($languages)){
		
		if( isset( $_POST[$languages[$i]] ) ){
			echo "<input type=\"checkbox\" name=$languages[$i] value=$languages[$i] checked/> $languages[$i]";
		}
		else {
			echo "<input type=\"checkbox\" name=$languages[$i] value=$languages[$i] /> $languages[$i]";
		}
		
		$i++;
	}
?>

<h1>Section II.</h1>
<p>
Failure to complete all items will prevent acceptance of this application.
</p>
<div <?php makeError("election") ?>>
1. 
SOCIAL SECURITY NUMBER* 
<input type="text" name="socialSecurityNumber" 
<?php makeSticky("socialSecurityNumber"); ?>
/>
</div>

<!-- Validates the SSN the user enetered. -->
<?php 

$num = '/^\d{3}-\d{2}-\d{4}$/';
if( isset( $_POST['socialSecurityNumber'])){
	if (! preg_match($num,$_POST['socialSecurityNumber']) ){
		echo "format must be in ddd-dd-dddd";
	}
}
?>
<br />
2. DATE OF BIRTH <select name="dateOfBirthMonth" onchange="addRemoveDays(this.value);">
<option value="1">January</option>
<option value="2">February</option>
<option value="3">March</option>
<option value="4">April</option>
<option value="5">May</option>
<option value="6">June</option>
<option value="7">July</option>
<option value="8">August</option>
<option value="9">September</option>
<option value="10">October</option>
<option value="11">November</option>
<option value="12">December</option>
</select>
<select name="dateOfBirthDay" id="DOBday">
<script type="text/javascript">
for(var day=1; day < 32; day++)
	document.write('<option value="'+day+'">'+day+'</option>\n');
</script>
</select>
<select name="dateOfBirthYear">
<script type="text/javascript">
var currentYear = new Date().getFullYear();
if (debug) window.alert("currentYear is "+currentYear);
for(var year=currentYear-17; year > currentYear-122; year--)
	document.write('<option value="'+year+'">'+year+'</option>\n');
</script>
</select>
<br />
3. 
<!-- Validates that a gender has been selected. -->
GENDER <input type="radio" name="gender" value ="male" <?php if(isset($_POST["gender"])){if($_POST["gender"] == "male"){echo "checked";}} ?>/> 
<!-- If user hasn't selected a gender, apply bold and red to Male -->
Male <input type="radio" name="gender" value ="female" <?php if(isset($_POST["gender"])){if($_POST["gender"] == "female"){echo "checked";}} ?> /> 
<!-- If user hasn't selected a gender, apply bold and red to Female -->
Female<br />
4. TELEPHONE Home: <input type="text" name="homePhone" 


<?php makeSticky("homePhone"); ?>
/>

<!-- Makes telphone number sticky -->
Business: <input type="text" name="businessPhone" 
<?php makeSticky("businessPhone");?>
/>

<!-- Outputs a specific error on the form if either home or business telephone entered -->
<?php 
if (isset($_POST['homePhone']) || isset($_POST['businessPhone'])){
	$num = '/^\d{3}-\d{4}$/';
	$statement = "format must be in ddd-dddd";
	if (preg_match($num,$_POST['homePhone']) || preg_match($num,$_POST['businessPhone'])){
		$statement = "";
		}
	echo $statement;
}


?>
<br />
5. 
LAST NAME <input type="text" name="lastName" 
<?php makeSticky("lastName"); ?>
/> 
 
FIRST NAME <input type="text" name="firstName" 
<?php makeSticky("firstName");?>
/> 
MIDDLE INITIAL(S) <input type="text" size="8" name="middleInitials" 
<?php makeSticky("middleInitials");	?>
/>

<!-- Checks name inputs for validity, else returns an error -->
<?php 
if (isset($_POST['lastName']) && isset($_POST['firstName'])){
	$key = '/[A-Za-z,\.]+/';
	$statement = "Name can only have Letters and Numbers. Please check your input";
	if (preg_match($key, $_POST['lastName'] ) && preg_match($key, $_POST['firstName'])){
		$statement = "";
	}
	echo $statement;
}
?>

<br />
<br />
6. 
RESIDENCE ADDRESS IN HAWAII (Must be completed. P.O. Box, R.R., S.R. are <strong>not</strong> acceptable)<br />
<input type="text" size="40" name="residenceAddress" <?php makeSticky("residenceAddress"); ?>/>
APT. NO. 
<input type="text" size="5" name="residenceAptNo" <?php makeSticky("residenceAptNo"); ?>/>
CITY/TOWN<input type="text" size="20" name="residenceCityTown" onchange="validateName(this.value, true);" <?php makeSticky("residenceCityTown"); ?>/>
ZIP CODE<input type="text" size="12" name="residenceZipCode" onchange="validateHIZipCode(this.value, true);" <?php makeSticky("residenceZipCode"); ?>/>
<br />
<br />
7. MAILING ADDRESS IN HAWAII (Street Address or P.O. Box)
<br />
<input type="text" size="40" name="mailingAddress" <?php makeSticky("mailingAddress"); ?>/>
CITY/TOWN<input type="text" size="20" name="mailingCityTown" onchange="validateName(this.value, true);" <?php makeSticky("mailingCityTown"); ?>/>
ZIP CODE<input type="text" size="12" name="mailingZipCode" onchange="validateHIZipCode(this.value, true);" <?php makeSticky("mailingZipCode"); ?>/>
<br />
<br />
8. 
If no street/residence address, describe location of residence (Leave blank if #6 is completed)<br />
<input type="text" name="residenceDescription" <?php makeSticky("residenceDescription"); ?>/>
CITY/TOWN<input type="text" name="descriptionCityTown" onchange="validateName(this.value, true);" <?php makeSticky("descriptionCityTown"); ?>/>
ZIP CODE<input type="text" name="descriptionZipCode" onchange="validateHIZipCode(this.value, true);" <?php makeSticky("descriptionZipCode"); ?>/>
<h1>Section III.</h1>
Please mail my ballots to: 
<br />
PRIMARY<table>
<tr>
<td>9. 
NAME </td>
<td><input type="text" name="mailToPrimaryName" size="52" onchange="validateName(this.value, true);" <?php makeSticky("mailToPrimaryName"); ?>/>
</td>
</tr>
<tr>
<td>10. 
FORWARDING<br>ADDRESS<br> 
(Include Zip Code)</td>
<td><textarea rows="4" cols="50" name="mailToPrimaryAddress" <?php makeSticky("mailToPrimaryAddress"); ?>></textarea></td>
</tr>
</table>
<input type="checkbox" name="holdPrimary" /> HOLD for Arrival 
<br>
<br>
GENERAL (if mailing address is different from PRIMARY)
<br />
<input type="checkbox" name="sameAddress" onclick="copyAddress();" /> same as PRIMARY
<table>
<tr>
<td>11. 
NAME </td>
<td><input type="text" name="mailToGeneralName" size="52" onchange="validateName(this.value, true);" <?php makeSticky("mailToGeneralName"); ?>/>
</td>
</tr>
<tr>
<td>12. 
<!-- Ensures the validation of number 12 of the form. -->
FORWARDING<br>ADDRESS<br> (Include Zip Code)</td>
<td><textarea rows="4" cols="50" name="mailToGeneralAddress" <?php makeSticky("mailToPrimaryAddress"); ?>></textarea></td>
</tr>
</table>
<input type="checkbox" name="holdGeneral" /> HOLD for Arrival 
<h1>Section IV.</h1>
13. <input type="checkbox" name="affirm" id="affirm" value="affirm" /> 
<!-- Apply bold, red formatting to the affirmation checkbox if box wasn't checked -->
I hereby affirm that: 1) I am the person named above; 2) I am requesting an absentee ballot for myself and no other; and 3) all information furnished on this application is true and correct.<br>
<!-- Hidden variable to check if the form was sent. -->
<input type="hidden" name="sent" value="true">
<input type="submit" value="Submit"/>
<p>
*Notice: A Social Security Number is required by HRS &sect;11-15 and HRS &sect;15-4. It is used to prevent fraudulent registration and voting. Failure to furnish this information will prevent acceptance of this application. Pursuant to HRS &sect;11-20, the City/County Clerks may use this application to transfer a voter to the proper precinct to correspond with the address given above, under item 6.
</p>
<!-- Finally output the sending message if the form has been sent and there were no errors. -->
</form>
</body>
</html>
